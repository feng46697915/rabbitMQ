package com.bwie.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bwie.mq.MQProducerImpl;

@Controller
public class MQController {
	
	@Autowired
	private MQProducerImpl mqProducer;
	
	@RequestMapping("showInfo")
	public String showInfo() {
		mqProducer.sendDataToQueue("my_second_spring_queue", "�й�");
		return "/index.jsp";
	}
}
