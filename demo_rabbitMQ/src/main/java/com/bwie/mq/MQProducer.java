package com.bwie.mq;

public interface MQProducer {
	
	/**
	 * 发送消息到指定队列
	 * 
	 * @param queueKey 队列名称
	 * @param object 发送内容
	 */
	public void sendDataToQueue(String queueKey, Object object);
	
}
