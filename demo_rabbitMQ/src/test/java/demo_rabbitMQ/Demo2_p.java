package demo_rabbitMQ;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class Demo2_p {
	
	public static void main(String[] args) {
		String QUEUE_NAME = "second_queue";
		try {
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost("127.0.0.1");
			
			Connection connection = factory.newConnection();
			
			Channel channel = connection.createChannel();
			
			channel.queueDeclare(QUEUE_NAME, false, false, false, null);
			
			for (int i = 0; i < 10; i++) {
				channel.basicPublish("", QUEUE_NAME, null, ("Hello��" + i + "���ձ�").getBytes());
			}
			
			channel.close();
			connection.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
	}
}
