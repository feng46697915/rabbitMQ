package demo_rabbitMQ;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

public class Demo3_c1 {
	   private final static String QUEUE_NAME = "queue_fanout_1";
	    private final static String EXCHANGE_NAME = "exchange_fanout";

	    public static void main(String[] argv) throws Exception {

			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost("127.0.0.1");
			
			Connection connection = factory.newConnection();
			
	        Channel channel = connection.createChannel();
	        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

	        // 绑定队列到交换机. 绑定也可在rabbitMQ的管理界面进行
	        channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "");

	        channel.basicQos(1);
	        QueueingConsumer consumer = new QueueingConsumer(channel);
	        channel.basicConsume(QUEUE_NAME, false, consumer);

	        while (true) {
	            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
	            String message = new String(delivery.getBody());
	            System.out.println("[1]获取:" + message);
	            Thread.sleep(100);
	            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
	        }
	    }
}
