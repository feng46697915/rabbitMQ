package demo_rabbitMQ;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

public class Demo1_c {
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		String QUEUE_NAME = "first_queue";
		try {
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost("127.0.0.1");
			
			Connection connection = factory.newConnection();
			
			Channel channel = connection.createChannel();
			
			channel.queueDeclare(QUEUE_NAME, false, false, false, null);
			
			channel.basicGet(QUEUE_NAME, true);
			
			// 定义队列的消费者
//			QueueingConsumer consumer = new QueueingConsumer(channel);
//			
//			channel.basicConsume(QUEUE_NAME, true, consumer);  //true 自动确认消息, 下有详解
			
//			Delivery delivery = consumer.nextDelivery();
//			
//			System.out.println(new String(delivery.getBody()));
			
			channel.close();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
