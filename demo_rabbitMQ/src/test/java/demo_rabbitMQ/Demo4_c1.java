package demo_rabbitMQ;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

public class Demo4_c1 {
	
	   private final static String QUEUE_NAME = "queue_direct_1";
	    private final static String EXCHANGE_NAME = "exchange_direct";

	    public static void main(String[] argv) throws Exception {

			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost("127.0.0.1");
			
			Connection connection = factory.newConnection();
	        Channel channel = connection.createChannel();
	        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
	        // 绑定队列到交换机, BindingKey为 delete update
	        channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "update");
	        channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "delete");

	        channel.basicQos(1);
	        QueueingConsumer consumer = new QueueingConsumer(channel);
	        channel.basicConsume(QUEUE_NAME, false, consumer);

	        while (true) {
	            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
	            String message = new String(delivery.getBody());
	            System.out.println("第4个例子用户A，获取:" + message);
	            Thread.sleep(100);
	            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
	        }
	    }
}
